#Deploying a React App* to GitHub Pages

    - https://github.com/gitname/react-gh-pages


#Generate a production build of your app, and deploy it to GitHub Pages. (2 minutes)

    - npm run deploy


#After you code, you want to deploy ?

    - git add .
    
    - git commit -m "Create a React app and publish it to GitHub Pages"
    
    - git push origin master