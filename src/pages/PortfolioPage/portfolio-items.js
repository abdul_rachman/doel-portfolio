import React from 'react';

export default [
  {
  name: 'typy',
  render: (
    <div className='portfolio-item'>
      <div className='portfolio-item__title'>
        Document Management System
      </div>
      <div className='portfolio-item__desc'>
        ***
      </div>
      <div className='portfolio-item__icon'>
        <i className="fab fa-html5"></i>
        {/* <i className="fab fa-js"></i> */}
        {/* <i className="fab fa-npm"></i> */}
      </div>
      <div className='portfolio-item__links'>
        {/* <a target="_blank" rel="noopener noreferrer" href="https://www.npmjs.com/package/typy">NPM</a> */}
        {/* <a target="_blank" rel="noopener noreferrer" href="https://github.com/flexdinesh/typy">More</a> */}
        <a target="_blank" rel="noopener noreferrer" href="https://drive.google.com/file/d/1LxHf4cQqbiS3Eq2jXBwVj9bldJ1ELJA-/view">Forbidden &raquo;&raquo;</a>
        
      </div>
    </div>
  )
},
{
  name: 'typy',
  render: (
    <div className='portfolio-item'>
      <div className='portfolio-item__title'>
        <h2>805 Carwash</h2>
      </div>
      <div className='portfolio-item__desc'>
        ***
      </div>
      <div className='portfolio-item__icon'>
        <i className="fab fa-node-js"></i>
        <i className="fab fa-react"></i>
        <i className="fab fa-html5"></i>
      </div>
      <div className='portfolio-item__links'>
        {/* <a target="_blank" rel="noopener noreferrer" href="https://www.npmjs.com/package/typy">NPM</a> */}
        <a target="_blank" rel="noopener noreferrer" href="http://128.199.177.16:7600/customer/login">Lebih lanjut &raquo;&raquo;</a>
        
        
      </div>
    </div>
  )
}, 
{
  name: 'typy',
  render: (
    <div className='portfolio-item'>
      <div className='portfolio-item__title'>
        <h2>Prosehat</h2>
      </div>
      <div className='portfolio-item__desc'>
        ***
      </div>
      <div className='portfolio-item__icon'>
        <i className="fab fa-wordpress"></i>
        <i className="fab fa-html5"></i>
      </div>
      <div className='portfolio-item__links'>
        {/* <a target="_blank" rel="noopener noreferrer" href="https://www.npmjs.com/package/typy">NPM</a> */}
        {/* <a target="_blank" rel="noopener noreferrer" href="https://github.com/flexdinesh/typy">More</a> */}
        <a target="_blank" rel="noopener noreferrer" href="https://www.prosehat.com/">Lebih lanjut &raquo;&raquo;</a>
        
      </div>
    </div>
  )
}, 
{
  name: 'typy',
  render: (
    <div className='portfolio-item'>
      <div className='portfolio-item__title'>
        <h2>MCS</h2>
      </div>
      <div className='portfolio-item__desc'>
        ***
      </div>
      <div className='portfolio-item__icon'>
        <i className="fab fa-free-code-camp"></i>
        <i className="fab fa-html5"></i>
      </div>
      <div className='portfolio-item__links'>
        {/* <a target="_blank" rel="noopener noreferrer" href="https://www.npmjs.com/package/typy">NPM</a> */}
        {/* <a target="_blank" rel="noopener noreferrer" href="https://github.com/flexdinesh/typy">More</a> */}
        <a target="_blank" rel="noopener noreferrer" href="http://www.mcs.co.id/">Lebih lanjut &raquo;&raquo;</a>
        
      </div>
    </div>
  )
}, 


]